#!/bin/sh
if command -v greadlink &> /dev/null; then
  SCRIPTDIR=`dirname "$(greadlink -f "$0")"`
else
  SCRIPTDIR=`dirname "$(readlink -f "$0")"`
fi

echo -n 'SUBMODULE Config... '
cd $SCRIPTDIR && git submodule update --init --recursive
echo 'OK'

echo -n 'SSH Config... '
mkdir -p ~/.ssh
ln -fs $SCRIPTDIR/sshconfig ~/.ssh/config
chmod 600 ~/.ssh/*
echo 'OK'

# echo -n 'READLINE Config... '
# ln -fs $SCRIPTDIR/inputrc ~/.inputrc
# echo 'OK'

# echo -n 'BASH Config... '
# ln -fs $SCRIPTDIR/bashrc ~/.bashrc
# ln -fs $SCRIPTDIR/bash_profile ~/.bash_profile
# echo 'OK'

# echo -n 'MINTTY Config... '
# ln -fs $SCRIPTDIR/minttyrc ~/.minttyrc
# echo 'OK'

# echo -n 'GIT Config... '
# ln -fs $SCRIPTDIR/gitconfig ~/.gitconfig
# mkdir -p $SCRIPTDIR/git-bash-completion
# wget -q -O $SCRIPTDIR/git-bash-completion/git-completion.bash \
#   https://raw.githubusercontent.com/git/git/master/contrib/completion/git-completion.bash
# echo 'OK'

# echo -n 'SCREEN Config... '
# ln -fs $SCRIPTDIR/screenrc ~/.screenrc
# echo 'OK'

# echo -n 'TMUX Config... '
# test -f ~/.tmux && rm ~/.tmux
# ln -fs $SCRIPTDIR/tmux ~/.tmux
# ln -fs $SCRIPTDIR/tmuxconfig ~/.tmux.conf
# echo 'OK'

# echo -n 'DIRCOLORS Config...'
# mkdir -p ~/.dircolors
# test -f ~/.dircolors/dircolors.256dark        && rm ~/.dircolors/dircolors.256dark
# test -f ~/.dircolors/dircolors.ansi-dark      && rm ~/.dircolors/dircolors.ansi-dark
# test -f ~/.dircolors/dircolors.ansi-light     && rm ~/.dircolors/dircolors.ansi-light
# test -f ~/.dircolors/dircolors.ansi-universal && rm ~/.dircolors/dircolors.ansi-universal
# ln -fs $SCRIPTDIR/dircolors-solarized/dircolors.256dark        ~/.dircolors/dircolors.256dark
# ln -fs $SCRIPTDIR/dircolors-solarized/dircolors.ansi-dark      ~/.dircolors/dircolors.ansi-dark
# ln -fs $SCRIPTDIR/dircolors-solarized/dircolors.ansi-light     ~/.dircolors/dircolors.ansi-light
# ln -fs $SCRIPTDIR/dircolors-solarized/dircolors.ansi-universal ~/.dircolors/dircolors.ansi-universal
# echo 'OK'
