@ECHO OFF

echo SSH Config...
md "%home%\.ssh" >nul 2>&1
del "%home%\.ssh\config" >nul 2>&1
mklink "%home%\.ssh\config" "%~dp0\sshconfig"

echo READLINE Config...
del "%home%\.inputrc" >nul 2>&1
mklink "%home%\.inputrc" "%~dp0\inputrc"

echo BASH Config...
del "%home%\.bashrc" >nul 2>&1
mklink "%home%\.bashrc" "%~dp0\bashrc"
del "%home%\.bash_profile" >nul 2>&1
mklink "%home%\.bash_profile" "%~dp0\bash_profile"

echo GIT Config...
del "%home%\.gitconfig" >nul 2>&1
mklink "%home%\.gitconfig" "%~dp0\gitconfig"

echo SUMATRA Config...
md "%appdata%\SumatraPDF" >nul 2>&1
del "%appdata%\SumatraPDF\SumatraPDF-settings.txt" >nul 2>&1
mklink "%appdata%\SumatraPDF\SumatraPDF-settings.txt" "%~dp0\sumatraconfig" >nul 2>&1

echo MINTTY Config...
del "%home%\.minttyrc" >nul 2>&1
mklink "%home%\.minttyrc" "%~dp0\minttyrc"
md "%appdata%\mintty" >nul 2>&1
del "%appdata%\mintty\config" >nul 2>&1
mklink "%appdata%\mintty\config" "%~dp0\minttyrc"

echo WSLTTY Config...
md "%appdata%\wsltty" >nul 2>&1
del "%appdata%\wsltty\config" >nul 2>&1
mklink "%appdata%\wsltty\config" "%~dp0\minttyrc"

echo LATEXMK Config...
del "%home%\.latexmkrc" >nul 2>&1
mklink "%home%\.latexmkrc" "%~dp0\latexmkrc"
