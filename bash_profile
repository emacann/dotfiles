# Return immediately if not running interactively
[ -z "$PS1" ] && return

# Source .bashrc
test -f ~/.bashrc && source ~/.bashrc
