# Return immediately if not running interactively
[ -z "$PS1" ] && return

# BASHRC_PRE Standard Settings
test -f  ~/.bashrc_pre && \
  source ~/.bashrc_pre

export LANG="en_US.UTF-8"
export LC_ALL="en_US.UTF-8"
export EDITOR="nvim"
export XDG_CONFIG_HOME="$HOME/.config"
export PATH="$HOME/.prefix/bin:$PATH"
export MANPATH="$HOME/.prefix/share/man/:$MANPATH"

# History Settings
HISTCONTROL=ignoreboth:erasedups
HISTSIZE=10000
HISTFILESIZE=20000
shopt -s histappend

# Check window size after each command and update values properly
shopt -s checkwinsize

# Use extended pattern matching
shopt -s extglob

# Disable command-not-found
unset command_not_found_handle

# Configuration Aliases
alias recfg='bind -f ~/.inputrc && source ~/.bashrc'

# Coloured Aliases
alias ls='ls -h --color=auto'
alias ll='ls -h -lF --color=auto'
alias la='ls -h -AlF --color=auto'
alias dir='dir --color=auto'
alias tree='tree -C'
alias vdir='vdir --color=auto'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'

# Disk Aliases
alias du='du -h'
alias df='df -h'

# Common aliases
alias so='source'
alias md='mkdir'
alias topme='top -u `whoami`'
alias htopme='htop -u `whoami`'

# WSL Settings
if grep -Rq "Microsoft" /proc/version; then
  export DISPLAY="localhost:0.0"
fi

# Cygwin Settings
if grep -Rq "Cygwin" /proc/version; then
  export DISPLAY="localhost:0.0"
fi

# Protect the accidental press of CTRL-D
export IGNOREEOF=4

# PS1 Settings
WinTitle='\[\e]0;\u@\H | \w\a\]'
WorkDirR='\[\e[01;35m\]\W\[\e[m\]'
export PS1="$WinTitle$WorkDirR > "

# Load dircolors
export DIRCOLORS=$HOME/.dircolors/dircolors.ansi-universal
test -f $DIRCOLORS && eval `dircolors $DIRCOLORS`

# Proper autocompletion
bind '\C-n':menu-complete
bind '\C-p':menu-complete-backward
bind "set menu-complete-display-prefix off"
bind "set show-all-if-ambiguous on"
